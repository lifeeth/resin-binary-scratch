package main

import (
        "fmt"
        "net/http"
        "flag"

        "github.com/zenazn/goji"
        "github.com/zenazn/goji/web"
)

func hello(c web.C, w http.ResponseWriter, r *http.Request) {
        fmt.Fprintf(w, "Hello, %s!", c.URLParams["name"])
}

func index(c web.C, w http.ResponseWriter, r *http.Request) {
        fmt.Fprintf(w, "Hello, World!")
}

// The below is because of how resin currently starts the container
var unusedflag string
func init() {
        flag.StringVar(&unusedflag, "c", "start","Ignored for now")
}

func main() {
        goji.Get("/:name", hello)
        goji.Get("/", index)
        goji.Serve()
}
        
